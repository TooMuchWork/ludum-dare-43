﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    public float explosionRadius;
    public float explosionForce;

    private void Explode()
    {
        Collider2D[] exploded = Physics2D.OverlapCircleAll(transform.position, explosionRadius);
        Vector2 shootDir = Vector2.zero;

        foreach (Collider2D obj in exploded)
        {
            if (obj.gameObject.layer == 10)
            {
                shootDir = obj.transform.position - transform.position;
                Shoot(shootDir, obj.GetComponentInParent<Rigidbody2D>(), explosionForce);
            }
        }

        //Explosion FX
        Destroy(gameObject);
    }

    public void Shoot(Vector2 dir, Rigidbody2D rb, float force)
    {
        rb.AddForce(dir.normalized * force, ForceMode2D.Impulse);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 10) //layer 10 - controller
        {
            Explode();
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}
