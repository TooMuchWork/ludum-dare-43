﻿using UnityEngine;
using UnityEngine.UI;

public class EndStageManager : MonoBehaviour
{
    public Text time;
    public Text sacrificies;

	public void SetNumbers(float _time, int _sacrificies)
    {
        time.text = "Time: " + _time.ToString("#.00");
        sacrificies.text = "Sacrificies: " + _sacrificies;
    }

    public void NextStage()
    {
        GameMaster.instance.NextStage();
    }
}
