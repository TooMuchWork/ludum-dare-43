﻿using System.Collections.Generic;
using UnityEngine;

public class SpawnPool : MonoBehaviour
{
    [System.Serializable]
    public class Pool
    {
        public string name;
        public GameObject prefab;
        public int objectsNumber;
        public bool expanding;

        public Queue<GameObject> queue = new Queue<GameObject>();
    }

    public List<Pool> pools = new List<Pool>();

    #region Singleton
    public static SpawnPool instance = null;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }
    #endregion

    private void Start()
    {
        foreach(Pool pool in pools)
        {
            InitializePool(pool);
        }
    }

    private void InitializePool(Pool pool)
    {
        for(int i = 0; i < pool.objectsNumber; i++)
        {
            GameObject obj = Instantiate(pool.prefab);
            obj.SetActive(false);

            pool.queue.Enqueue(obj);
        }
    }

    public Pool FindPoolByName(string name)
    {
        foreach(Pool pool in pools)
        {
            if(pool.name == name)
            {
                return pool;
            }
        }

        return null;
    }

    public Pool FindPoolByPrefab(GameObject prefab)
    {
        foreach (Pool pool in pools)
        {
            if (pool.prefab.Equals(prefab))
            {
                return pool;
            }
        }

        return null;
    }

    public GameObject Spawn(string poolName, Vector3 position, Quaternion rotation)
    {
        Pool pool = FindPoolByName(poolName);

        GameObject objectToSpawn = pool.queue.Dequeue();

        if (!pool.expanding || !objectToSpawn.activeSelf)
        {
            objectToSpawn.SetActive(true);
            objectToSpawn.transform.position = position;
            objectToSpawn.transform.rotation = rotation;  
        }
        else if(pool.expanding && objectToSpawn.activeSelf)
        {
            GameObject newObj = Instantiate(pool.prefab, position, rotation);
            pool.queue.Enqueue(newObj);
            return newObj;
        }

        pool.queue.Enqueue(objectToSpawn);
        return objectToSpawn;
    }

    public GameObject Spawn(GameObject prefab, Vector3 position, Quaternion rotation)
    {
        Pool pool = FindPoolByPrefab(prefab);

        GameObject objectToSpawn = pool.queue.Dequeue();

        if (!pool.expanding || !objectToSpawn.activeSelf)
        {
            objectToSpawn.SetActive(true);
            objectToSpawn.transform.position = position;
            objectToSpawn.transform.rotation = rotation;
        }
        else if (pool.expanding && objectToSpawn.activeSelf)
        {
            GameObject newObj = Instantiate(pool.prefab, position, rotation);
            pool.queue.Enqueue(newObj);
            return newObj;
        }

        pool.queue.Enqueue(objectToSpawn);
        return objectToSpawn;
    }
}