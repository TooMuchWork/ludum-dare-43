﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    private float timer = 0;
    private Rigidbody2D rb;

    public float lifetime, speed;

	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();

        rb.velocity = -Vector2.right * speed;
	}

    void Update ()
    {
        timer += Time.deltaTime;

        if (timer > lifetime)
        {
            gameObject.SetActive(false);
        }
    }

    private void OnEnable()
    {
        timer = 0;
        if(rb != null)
            rb.velocity = -Vector2.right * speed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.layer == 9)
        {
            gameObject.SetActive(false);
        }
    }
}
