﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    public float fireRate;
    public Transform gunpoint;

    private float timer = 0;

	void Update ()
    {
        timer += Time.deltaTime;

        if(timer > fireRate)
        {
            Shoot();
            timer = 0;
        }
	}

    void Shoot()
    {
        SpawnPool.instance.Spawn("Bullets", gunpoint.position, gunpoint.rotation);
        SpawnPool.instance.Spawn("ShootSFX", transform.position, Quaternion.identity).GetComponent<AudioSource>().Play(0);
    }
}
