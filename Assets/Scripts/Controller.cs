﻿using System.Collections;
using UnityEngine;

public class Controller : MonoBehaviour
{
    private Rigidbody2D rb;
    private Animator animator;
    private string damagableLayerName = "Damagable";
    private string stageEndTag = "StageEndFlag";
    private int groundLayerName = 9;

    #region Inputs
    private string horizontalAxis = "Horizontal";
    private string verticalAxis = "Vertical";

    private float horizontal_raw;
    private float vertical_raw;

    private bool spaceDown, esc;
    #endregion
    [Header("Death")]
    public Sprite deadBody;

    #region Movement
    [Header("Movement")]
    public GameObject sprite;
    public GameObject feet;
    public LayerMask ground;
    public float speed;
    public float groundCheckRadius;
    #endregion

    #region Jump
    [Header("Jump")]
    public float jumpForce;
    public float fallMultiplier;
    public float jumpMultiplier;
    public float maxJump;
    #endregion

    #region States
    [Header("States")]
    public bool isAlive;
    public bool canMove;
    public bool isGrounded;
    public bool airControl;
    #endregion

    void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = sprite.GetComponent<Animator>();
	}

    private void Update()
    {
        if (!isAlive) return;

        horizontal_raw = Input.GetAxisRaw(horizontalAxis);
        vertical_raw = Input.GetAxisRaw(verticalAxis);

        spaceDown = Input.GetButton("Jump");
        esc = Input.GetKeyDown(KeyCode.Escape);

        isGrounded = IsGrounded();

        Animate();

        if (esc) GameMaster.instance.LoadScene(0);
    }

    void FixedUpdate ()
    {
        if (!isAlive) return;

        Movement();
        Jump();
        Flip();
	}

    void Movement()
    {
        if (!isGrounded && !airControl || !canMove) return;

        Vector2 dir = new Vector2(horizontal_raw, rb.velocity.y);
        Vector2 move = new Vector2(dir.x * speed, rb.velocity.y);

        rb.velocity = move;
    }

    void Jump()
    {
        if (!canMove) return;

        if (rb.rotation != 0)
        {
            rb.rotation = 0;
        }

        if (isGrounded && spaceDown)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            SpawnPool.instance.Spawn("JumpSFX", transform.position, Quaternion.identity).GetComponent<AudioSource>().Play(0);
        }

        if(rb.velocity.y < 0 && !isGrounded)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
        else if (rb.velocity.y > 0 && !spaceDown)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (jumpMultiplier - 1) * Time.deltaTime;
        }
    }

    void Flip()
    {
        if(horizontal_raw > 0) sprite.GetComponent<SpriteRenderer>().flipX = false;
        else if(horizontal_raw < 0) sprite.GetComponent<SpriteRenderer>().flipX = true;
    }

    void Death()
    {
        isAlive = false;

        rb.velocity *= .5f; // slow down after death so corpses won't fly off too far away
        animator.enabled = false;

        GameMaster.instance.sacrificies += 1;
        Debug.Log(GameMaster.instance.sacrificies);
        sprite.GetComponent<SpriteRenderer>().sprite = deadBody;

        foreach (Transform child in transform)
        {
            child.gameObject.layer = groundLayerName;
        }
    }

    void Animate()
    {
        animator.SetFloat("Velocity", horizontal_raw);
        animator.SetBool("IsGrounded", isGrounded);
    }

    bool IsGrounded()
    {
        bool isGrounded = false;

        Collider2D hit = Physics2D.OverlapCircle(feet.transform.position, groundCheckRadius, ground);
        if (hit != null) isGrounded = true;

        return isGrounded;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer(damagableLayerName))
        {
            if (isAlive && canMove)
            {
                SpawnPool.instance.Spawn("HitSFX", transform.position, Quaternion.identity).GetComponent<AudioSource>().Play(0);
                SpawnPool.instance.Spawn("BloodFX", transform.position, Quaternion.identity);
                Death();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == stageEndTag)
        {
            if(isAlive)
                StartCoroutine(NextStage());   
        }
    }

    IEnumerator NextStage()
    {
        SpawnPool.instance.Spawn("EndStageSFX", transform.position, Quaternion.identity).GetComponent<AudioSource>().Play(0);
        yield return new WaitForSeconds(.3f);
        GameMaster.instance.StageEnd();
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(feet.transform.position, groundCheckRadius);
    }
}
