﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public Transform target;
    public float smoothness;
    public bool followX, followY;

	void FixedUpdate ()
    {
        if (target != null)
        {
            Vector3 newPosition = Vector3.zero;

            if (followX) newPosition.x = target.position.x;
            if (followY) newPosition.y = target.position.y;

            transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime * smoothness);
        }
    }
}
