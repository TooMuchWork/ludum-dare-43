﻿using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
    #region Spawns
    private const string playerSpawnPointTag = "PlayerSpawnPoint";
    private const string playerCharactersPoolName = "PlayerCharacters";

    private GameObject playerSpawnPoint;
    #endregion

    private float timer, elapsedTime;
    private bool startTimer;
    public int neccesities, sacrificies;

    public GameObject endStageScreen;
    public GameObject currPlayerCharacter;

    #region Singleton
    public static GameMaster instance = null;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }
    #endregion

    private void Start()
    {
        StartCoroutine(StartGame());
        sacrificies = 0;
        timer = 0;
    }

    IEnumerator StartGame()
    {
        yield return new WaitForSecondsRealtime(.5f);
        SpawnPlayerCharacter();
    } 

    void Update ()
    {
        SpawnAnother();
	}

    private void LateUpdate()
    {
        StartTimer();
        TimerTicking();
    }

    void SpawnAnother()
    {
        if (currPlayerCharacter == null) return;

        bool alive = currPlayerCharacter.GetComponent<Controller>().isAlive;

        if (!alive)
        {
            currPlayerCharacter = SpawnPool.instance.Spawn(playerCharactersPoolName, playerSpawnPoint.transform.position, Quaternion.identity);
        }
    }

    void SpawnPlayerCharacter()
    {
        playerSpawnPoint = GameObject.FindWithTag(playerSpawnPointTag);
        currPlayerCharacter = SpawnPool.instance.Spawn(playerCharactersPoolName, playerSpawnPoint.transform.position, Quaternion.identity);
    }

    public void StartTimer()
    {
        if (Input.anyKeyDown && !startTimer)
        {
            Debug.Log("Start Timer");
            startTimer = true;
        }
    }

    void TimerTicking()
    {
        if(startTimer)
            timer += Time.deltaTime;
    }

    public void LoadScene(int i)
    {
        SceneManager.LoadScene(i);
    }

    public void StageEnd()
    {
        startTimer = false;

        endStageScreen.transform.GetChild(0).GetComponent<EndStageManager>().SetNumbers(timer, sacrificies);

        Instantiate(endStageScreen);
        currPlayerCharacter.GetComponent<Controller>().canMove = false;
    }

    public void NextStage()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

}